# MidiDiff

this project is created by [JUCE](https://juce.com/), based on the [MidiLoggerPluginDemo](https://github.com/juce-framework/JUCE/blob/master/examples/Plugins/MidiLoggerPluginDemo.h)

## About
This VST plugin calculates the differences of two midi channels and provides a result in percentage. 
